package main

import (
	"fmt"
	"go/bigint/bigint"
)

func main() {

	a, err := bigint.NewInt("1")
	if err != nil {
		panic(err)
	}
	b, err := bigint.NewInt("123456789")
	if err != nil {
		panic(err)
	}

	err = a.Set("987654321")
	if err != nil {
		panic(err)
	}

	c := bigint.Add(a, b)
	d := bigint.Sub(a, b)
	e := bigint.Multiply(a, b)
	f := bigint.Mod(a, b)
	// Abs()

	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c) //add
	fmt.Println(d) //sub
	fmt.Println(e) //mult
	fmt.Println(f)
	// fmt.Println(g) //for Abs()
}
